// Script File

function showImage(fileInput) {
	if(fileInput.files && fileInput.files[0]) {

		$("#result").empty();
		$("#img").css('display', 'block');
		var reader = new FileReader();

		reader.onload = (e) => {
			$("#img").attr("src", e.target.result)
			$("span.file-name")[0].innerHTML = fileInput.files[0].name
		};

		reader.readAsDataURL(fileInput.files[0]);
	}

}

function sendFormData() {
	var formData = new FormData($('#form')[0]);
	$("#wait").append("and the prediction is... (wait a second)")

	$.ajax({
		type: 'POST',
		url: '/classify',
		data: formData,
		contentType: false,
		cache: false,
		processData: false,
		success: function(data) {
			$("#result").empty();
			$("#result").append(data)
			console.log('Success!');
		},
		error: function(data) {
			alert(data.responseText);
		}
	})

}

$(document).ready(function() {
	$("#file").change(function() {
		showImage(this);
	});

	$("#predict").on('click', sendFormData);
});