# Notes
Due to a slug size problem when deploying to Heroku, the lusty paddy website is currently not accessible to users.

## But, you can clone it locally..
1. type in terminal: git clone https://github.com/rafka-imandaputra/Lusty-Paddy.git <br>
2. create your virtual environment and activate it (search google instead how to create venv in python)
3. cd to your directory and type this in terminal: pip install -r requirements.txt
4. download our machine learning model here [Lusty Paddy Model](https://drive.google.com/file/d/1Jt0SaU9mTbqqiJLJ11r7XzAP6Dv7NVud/view?usp=sharing) and save to "model/" directory
5. type in terminal: flask run<br>
Voila! welcome to our website!

# Lusty Paddy
We make a web application that helps farmers to produce quality, disease-free rice with machine learning. It's a paddy (rice) disease classification and it will be useful to ensure health for the population, especially in the Asian region, which makes rice as a staple food.

## Our Goals
#### Ensuring good paddy quality
With its main features, Lusty Paddy is useful for farmers to produce rice from high-quality and disease-free paddy.
#### Mitigating paddy disease
If there are rice that suffers from disease, Lusty Paddy gives suggestions to mitigate the same thing happening to other paddy.
#### Supports healthy nutrition for social
With good quality paddy (rice), it will be useful to ensure health for the population, especially in the Asian region, which makes rice as a staple food.

## 1. Overview
#### 1.1. Problem framing
Rice (Oryza sativa) is one of the staple foods worldwide. Paddy, the raw grain before removal of husk, is cultivated in tropical climates, mainly in Asian countries. Paddy cultivation requires consistent supervision because several diseases and pests might affect the paddy crops, leading to up to 70% yield loss. Expert supervision is usually necessary to mitigate these diseases and prevent crop loss. With the limited availability of crop protection experts, manual disease diagnosis is tedious and expensive. Thus, it is increasingly important to automate the disease identification process by leveraging computer vision-based techniques that achieved promising results in various domains

#### 1.2. What data did you use to draw conclusions?
Training dataset of 10,407 (75%) rice leaf images labeled in ten classes (nine disease and normal leaves). From this data, we developed an accurate disease classification model using a training dataset and classified each rice leaf image test data uploaded to the website into one of nine diseases or normal leaves.

#### 1.3. What insights did you gain from analyzing the data?
Develop a machine learning or deep learning-based model to classify the given paddy leaf images accurately, it's around 97% accuracy.

#### 1.4. What is your conclusion about change in our society?
Helping farmers to produce quality, disease-free rice with machine learning. It's a paddy (rice) disease classification and it will be useful to ensure health for the population, especially in the Asian region, which makes rice as a staple food.

#### 1.5. Deployment
With heroku (Unfortunately, we have a slug size issue that prevents Lusty Paddy from being published in web app.)

## 2. Web Feature, Flow, Design, Code, Testing.
#### 2.1. Main goals
predict paddy disease; checking paddy quality; knowledge about paddy disease; making easy report.

#### 2.2. Feature
##### Landing page
User input: none. <br>
Web output: displays shortcuts to switch to the lusty paddy menu, in the form of quick mode, report mode, encyclopaddy, and about lusty paddy.

##### Quick mode
User input: form image upload<br>
Web output: paddy image, disease prediction result.

##### Report mode (coming soon)
User input: multiple image upload; add location each image; add additional notes.
Web output: paddy image; disease prediction result; location; additional notes. (with option pdf download)

#####  Encyclopaddy
User input: none.<br>
Web output: description types of paddy diseases.

##### About Lusty Paddy
User input: none.<br>
Web output: main objective of web app, background of the problem, data sources, future application development plans, meet the developer, credit section.

# Lusty Paddy Future Improvement
### Multiple image classifier
It is easier to track diseased paddy by inputting several images at once, along with location information, and additional notes. Accompanied by export to a pdf file.
### Real time classifier
Predict directly with more interactive and easy-to-use features.
### Upload to Cloud System
Trace paddy diseases and store the information in a database to be accessed at a later time.

# Credit Section
thanks to https://www.kaggle.com/competitions/paddy-disease-classification for the dataset we used in this project. <br>
thanks to https://www.kaggle.com/code/miquel0/paddy-doctor-resnet34-pytorch-w-b/data for provide pretrained model.
